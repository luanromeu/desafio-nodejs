# Desafio Quacke Parser

Api para expor resultados de partidas a partir de logs do game Quake

## Técnologias utilizadas

* [express](https://expressjs.com/pt-br/) - Framework NodeJS
* [Jest](https://jestjs.io/pt-BR/) - Framework de Testes
* [Nodemon](https://nodemon.io/) - Utilitário para atualizar automaticamente o server
* [Docker](https://www.docker.com/) - Automatização e configuração do ambiente de desenvolvimento
* [Apidoc](https://apidocjs.com/) - Utilitário para documentação da api
* [GitLab CI/CD](https://gitlab.com/) - Ferramenta para automatização de pipeline


### Pré-requisitos

```
NodeJS (Versão 8.0.0 ou acima)
Docker (é recomendado Versão 1.12 ou acima ) Opcional
```

### Para instalar e rodar o projeto utilizando Docker

Após realizar a clonagem do projeto, entre na pasta raiz e execute o código abaixo. 
O docker se encarregará da configuração do ambiente, instalar as dependencias necessárias, executar os testes unitários, e servir o projeto na porta 3000.

```
docker-compose up
ou
docker-compose up -d (caso deseje liberar o terminal após subir o servidor)
```


### Para instalar (modo tradicional)

Após realizar a clonagem do projeto, entre na pasta raiz aplicação e em seguida na pasta "api-data" e execute o comando abaixo.

```
npm install
```

Depois de finalizar a execução do código acima, inicie a aplicação executando o código abaixo

```
npm start
```

## Para executar os testes unitários

 A aplicação foi testada utilizando a bibilioteca Jest, para executar os testes, rode o comando abaixo no diretorio "api-data"
 
```
npm run test
```

## Para melhor visualização dos testes unitarios

 O framework jets nos fornece uma ferramenta visual de cobertura de testes,  para acessa-la navegue até o diretorio 
  "api-data\coverage\lcov-report".  Dentro desse diretorio se encontra um arquivo index.html,  que quando aberto no navegador
 deve gerar um resultado semelhantes com o exemplo da imagem abaixo
 
 * [Exemplo coverage](https://ibb.co/MMrYqq5)

## Documentação da api completa

Para ter acesso as rotas acesse a documentação da api, faça um dos passos acima para rodar o projeto e em seguida cole a url abaixo no seu navegador.

```
http://localhost:3000/apidoc/
```


##Segunda opção de documentação

**BUSCAR TODAS AS PARTIDAS**
**GET** 
```
http://localhost:3000/api/list  
```

**BUSCAR PARTIDAS ESPECIFICAS**
**GET**

```
http://localhost:3000/api/list/id?gameId={Id}
```

##Pipeline automatizado

#O Projeto possui pipeline automatizado, abaixo temos a imagem do pipeline completo após um novo commit.

* [Pipeline](https://imgbbb.com/image/L4w9V5)

## Author

* **Luan Romeu Dias de Lima**
