const request = require('supertest')
const app = require('../../src/app')

describe('Teste controller', () => {

  test('Listart todas Partidas retorna ok', async () => {
    const res = await request(app)
      .get('/api/list')
    expect(res.statusCode).toEqual(200)
  })

  test('Listar Partida por identificador retorna exatamente uma partida', async () => {
    const res = await request(app)
      .get('/api/list/id')
      .query({
        gameId: 1
      })
      expect(res.statusCode).toEqual(200)
      expect(res.body).toEqual(
        {
            "hostname": "Code Miner Server",
            "version": "ioq3 1.36 linux-x86_64 Apr 12 2009",
            "total_kills": 0,
            "players": [
                "Isgalamido"
            ],
            "kills": {
                "Isgalamido": 0
            }
        }
      )
  })

  test('Listar partida  por identificador não existente retorna erro', async () => {
      try {
        const res = await request(app)
        .get('/api/list/id')
        .query({  gameId: 0})
         expect(res.statusCode).toEqual(400);
      } catch (e) {
        expect(e).toEqual({
            error: 'Partida não encontrada',
          });
      }
  })

  test('Listart partidas passando endpoint inexistente retorna 404', async () => {
    const res = await request(app)
      .get('/api/list/games')
      expect(res.statusCode).toEqual(404)
  })

  afterAll(() => {
    done()
  });

})