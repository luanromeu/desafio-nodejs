const User = require('../../src/models/user')

describe('teste do model de usuario', () => {

    const lines = [
        " 20:38 ClientConnect: 2",
        " 21:51 ClientConnect: 3",
        " 20:38 ClientUserinfoChanged: 2 n\\Isgalamido\\t\\0\\model\\uriel/zael\\hmodel\\uriel/zael\\g_redteam\\\\g_blueteam\\\\c1\\5\\c2\\5\\hc\\100\\w\\0\\l\\0\\tt\\0\\tl\\0",
        " 21:51 ClientUserinfoChanged: 3 n\\Dono da Bola\\t\\0\\model\\sarge/krusade\\hmodel\\sarge/krusade\\g_redteam\\\\g_blueteam\\\\c1\\5\\c2\\5\\hc\\95\\w\\0\\l\\0\\tt\\0\\tl\\0",
        " 22:06 Kill: 2 3 7: Isgalamido killed Dono da Bola by MOD_ROCKET_SPLASH",
        " 22:07 Kill: 2 3 7: Isgalamido killed Dono da Bola by MOD_ROCKET_SPLASH",
        " 22:08 Kill: 2 3 7: Isgalamido killed Dono da Bola by MOD_ROCKET_SPLASH",
        " 22:09 Kill: 2 3 7: Isgalamido killed Dono da Bola by MOD_ROCKET_SPLASH"
      ]

      let user

      beforeEach(() => {
        user = new User(lines[0])
      })

      test('atualizar usuario a cada linha', () => {
        expect(user.username).toEqual('')
        user.update(lines[2])
        expect(user.username).toEqual('Isgalamido')
      })

})
