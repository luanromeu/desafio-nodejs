const Arena = require('../../src/models/arena')
const User = require('../../src/models/user')
  
describe('Teste do modelo Arena', () => {

    let arena;
    beforeEach(() => {
     arena = new Arena()
    })

    const InfoArena = {
        hostname: 'Code Miner Server',
        version: 'ioq3 1.36 linux-x86_64 Apr 12 2009'
      }

    const testFirstLines = [
        "  0:00 InitGame: \\sv_floodProtect\\1\\sv_maxPing\\0\\sv_minPing\\0\\sv_maxRate\\10000\\sv_minRate\\0\\sv_hostname\\Code Miner Server\\g_gametype\\0\\sv_privateClients\\2\\sv_maxclients\\16\\sv_allowDownload\\0\\dmflags\\0\\fraglimit\\20\\timelimit\\15\\g_maxGameClients\\0\\capturelimit\\8\\version\\ioq3 1.36 linux-x86_64 Apr 12 2009\\protocol\\68\\mapname\\q3dm17\\gamename\\baseq3\\g_needpass\\0",
        "  9:32 InitGame: \\sv_floodProtect\\1\\sv_maxPing\\0\\sv_minPing\\0\\sv_maxRate\\10000\\sv_minRate\\0\\sv_hostname\\Code Miner Server\\g_gametype\\0\\sv_privateClients\\2\\sv_maxclients\\16\\sv_allowDownload\\0\\dmflags\\0\\fraglimit\\20\\timelimit\\15\\g_maxGameClients\\0\\capturelimit\\8\\version\\ioq3 1.36 linux-x86_64 Apr 12 2009\\protocol\\68\\mapname\\q3dm17\\gamename\\baseq3\\g_needpass\\0",
      ]

      test('Le as 2 primeiras linhas e retorna informação do servidor do jogo', () => {
        const arena1 = new Arena(testFirstLines[0])
        const arena2 = new Arena(testFirstLines[1])
        expect([arena1.hostname, arena1.version]).toEqual([InfoArena.hostname, InfoArena.version])
        expect([arena2.hostname, arena2.version]).toEqual([InfoArena.hostname, InfoArena.version])
      })

      test('Adiciona uma morte na Arena', () => {
        expect(arena.total_kills).toEqual(0)
        arena.addKill()
        expect(arena.total_kills).toEqual(1)
      })

      let user1;
      let user2;

      const userLines = [
        " 20:34 ClientConnect: 2",
        " 12:34 ClientConnect: 3",
      ]

      beforeEach(() => {
        user1 = new User(userLines[0])
        user1.username = 'Paulo H. da Silva'
        user2 = new User(userLines[1])
        user2.username = 'Bárbara Lopes Milani'
      })

      test('Retorna o nome do jogador', () => {
        expect(arena.playersNames().length).toEqual(0)
        arena.newPlayer(user1)
        expect(arena.playersNames()).toEqual(['Paulo H. da Silva'])
        arena.newPlayer(user2)
        expect(arena.playersNames()).toEqual(['Paulo H. da Silva', 'Bárbara Lopes Milani'])
      })

})

  


