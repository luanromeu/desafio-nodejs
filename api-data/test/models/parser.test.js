describe('Parser', () => {
    const config = require('../../config')
    const Parser = require('../../src/models/parser')
  
    beforeEach(() => {
      parser = new Parser()
      parser.readFile(`${config.global.LOGGER_TXT}`)
    })
  
    it('Verifica se parser retorna informação esperada', () => {
      parserGames = parser.toObject()
      expect(Object.keys(parserGames).length).toEqual(21)
    })
  })