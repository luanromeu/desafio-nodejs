'use strict'

const Repository = require('../repository/arena')


exports.listAll = async(req, res, next) => {
    try {
        const result = await Repository.listAll();  
        if(result)
        res.status(200).send(result);
        else
        throw new Error();
    } catch (error) {
        res.status(400).send({
            error:"NotFound",
            message:"Nenhum resultado encontrado"
        });
    }
};

exports.listOne = async(req, res, next) => {
    try {
        let gameId = await req.query.gameId
        const result = await Repository.listOne(gameId);
        if(result)
        res.status(200).send(result);
        else
        throw new Error()
    } catch (error) {
        res.status(400).send({
            error:"RoundNotFound",
            message:"Partida não encontrada"
        });
    }
}
