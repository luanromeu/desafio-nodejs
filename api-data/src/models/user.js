const USER_WORLD_ID = 1022

class User {

  constructor ( line = '' ) {
    this.id = User.getPlayerId(line)
    this.username = ''
    this.kills = 0
    this.deadsByWorld = 0
  }

  static getPlayerId (line) {
    const regex = /Client(Connect|UserinfoChanged): ([0-9]*)/
    let playerId = line.match(regex)
    return playerId ? playerId[2] : 0
  }

  static new (parser, line) {
    let currentGame = parser.getCurrentGame()
    currentGame.newPlayer(new User(line))
  }

  static update (parser, line) {
    let currentGame = parser.getCurrentGame()
    let user = currentGame.getPlayerById(User.getPlayerId(line))

    if (user) {
      user.update(line)
    } 
  }

  static kill (parser, line) {
    let currentGame = parser.getCurrentGame()
      , regex = /Kill: ([0-9]+) ([0-9]+)/
      , players = line.match(regex) 
    ;

    if (players) {
      currentGame.addKill()
      if (players[1] == USER_WORLD_ID) {
        currentGame.players.get(players[2]).deadsByWorld++
      } else {
        currentGame.players.get(players[1]).addKill()
      }
    } 
  }

  calcScore () {
    const score = this.kills - this.deadsByWorld
    return score < 0 ? 0 : score
  }

  addKill () {
    this.kills++
  }

  removeKill () {
    const killsToBeRemoved = this.kills > 0 ? 1 : 0
    this.kills -= killsToBeRemoved
  }

  update (line) {
    this.username = line.match(/ClientUserinfoChanged: [0-9]* n\\(.*)\\t\\[0-9]+\\model/)[1]
  }

}

module.exports = User