const fs = require('fs')
const Game = require('./arena')
const Player = require('./user')
const GET_COMMAND = /^.{0,7}([a-z A-Z][^:]*)/

class Parser {

  constructor () {
    this.games = new Map()
    this.currentGame = 0
  }

  addGame(game) {
    this.currentGame++
    this.games.set(this.currentGame, game)
    return this
  }

  toObject() {
    let ret = {}
    this.games.forEach((item, idx) => {
      ret[`game_${parseInt(idx)}`] = {
        hostname: item.hostname,
        version: item.version,
        total_kills: item.total_kills,
        players: item.playersNames(),
        kills: item.playersKills()
      }
    })
    return ret
  }

  readFile (logFile) {
    let lines = fs.readFileSync(logFile).toString().split("\n")
    this.parseLines(lines)
  }


  parseLines (lines) {
    let command = ''
      , lastLine = lines.length
      , i
    ;

    for (i = 0; i < lastLine; i++) {
      command = lines[i].match(GET_COMMAND)
      if (!!command) {
        this.checkCommand(command[1], lines[i], i)
      }
    }
  }

  checkCommand (command, line, idx) {
    switch (command) {
      case 'InitGame':
        Game.new(this, line)
        break
      case 'ClientConnect':
        Player.new(this, line)
        break
      case 'ClientUserinfoChanged':
        Player.update(this, line)
        break
      case 'Kill':
        Player.kill(this, line)
        break
      default:
        break
    }
  }

  getCurrentGame () {
    return this.games.get(this.currentGame)
  }
}

module.exports = Parser
