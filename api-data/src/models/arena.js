class Arena {

    constructor (line = '') {
      this.players = new Map()
      this.total_kills = 0
      this.hostname = this.version = ''
      if (line.length > 0) {
        this.hostname = line.match(/sv_hostname\\([a-z A-Z 0-9][^\\]*)/)[1]
        this.version = line.match(/version\\(.*)\\protocol/)[1]
      }
    }
  

    static new (parser, line) {
      parser.addGame(new Arena(line))
    }
  
 
    addKill () {
      this.total_kills++
    }
  

    getPlayerById (id) {
      if (this.players.has(id)) {
        return this.players.get(id)
      }
      return null
    }
  

    newPlayer (player) {
      this.players.set(player.id, player)
    }

    playersNames () {
      let result = []
      this.players.forEach(player => {
        result.push(player.username)
      })
      return result
    }
  

    playersKills () {
      let result = {}
      this.players.forEach(player => {
        result[player.username] = player.calcScore()
      })
      return result
    }
  
  }
  
  module.exports = Arena