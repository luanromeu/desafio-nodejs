'use strict'
const Parser = require('../models/parser')
const config = require('../../config')
const parser = new Parser()
parser.readFile(`${config.global.LOGGER_TXT}`)

exports.listAll = async () => {
 return await parser.toObject(); 
}

exports.listOne = async (gameId) => {
return await parser.toObject()[`game_${gameId}`]; 
}

