'use strict'

const express = require('express');
const router = express.Router();
const controller = require('../controllers/arena');

router.get('/list', controller.listAll)
router.get('/list/id', controller.listOne)

module.exports = router;


 /**
 * @api {get} /api/list   Listar todas as partidas
 * @apiName  GET ALL
 * @apiGroup Arena
 *
 *
 * @apiSuccess {String} hostname  Identificador do servidor.
 * @apiSuccess {String} version  versão S.O do servidor.
 * @apiSuccess {Number} total_kills  total de mortes no servidor.
 * @apiSuccess {List}  players  nome dos players na partida.
 * @apiSuccess {Object} kills  quantidade de kills por usuario.
 * 
 * @apiSuccessExample Success-Response:
 *   {
    "game_1": {
        "hostname": "Code Miner Server",
        "version": "ioq3 1.36 linux-x86_64 Apr 12 2009",
        "total_kills": 0,
        "players": [
            "Isgalamido"
        ],
        "kills": {
            "Isgalamido": 0
        }
    },
    "game_2": {
        "hostname": "Code Miner Server",
        "version": "ioq3 1.36 linux-x86_64 Apr 12 2009",
        "total_kills": 11,
        "players": [
            "Isgalamido",
            "Mocinha"
        ],
        "kills": {
            "Isgalamido": 0,
            "Mocinha": 0
        }
    },
 *
 * @apiError NotFound Nenhum Resultado encontrado.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *        "error":"NotFound",
 *       "message":"Nenhum resultado encontrado"
 *     }
 */

 /**
 * @api {get} /api/list/id?gameId=${idDaPartida}    Listar partida por Id 
 * @apiName GET BY ID
 * @apiGroup Arena
 *
 *
 * @apiSuccess {String} hostname  Identificador do servidor.
 * @apiSuccess {String} version  versão S.O do servidor.
 * @apiSuccess {Number} total_kills  total de mortes no servidor.
 * @apiSuccess {List}  players  nome dos players na partida.
 * @apiSuccess {Object} kills  quantidade de kills por usuario.
 * 
 * @apiSuccessExample Success-Response:
 *   {
    "game_1": {
        "hostname": "Code Miner Server",
        "version": "ioq3 1.36 linux-x86_64 Apr 12 2009",
        "total_kills": 0,
        "players": [
            "Isgalamido"
        ],
        "kills": {
            "Isgalamido": 0
        }
    }
 *
 * @apiError RoundNotFound Partida não encontrada.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 400 Not Found
 *     {
 *        "error":"RoundNotFound",
 *       "message":"Partida não encontrada"
 *     }
 */